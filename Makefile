CC=gcc
CFLAGS=-ggdb3 -O0 -Wall -Wextra -pedantic -fPIC
LDFLAGS=-shared
SRCS = libgdbprint.c
OBJS = $(SRCS:.c=.o)
NAME=libgdbprint.so

all: $(NAME)

$(NAME): $(OBJS)
	$(CC) -o $(NAME) $(CFLAGS) $(LDFLAGS) $^

%.o : %.c
	$(CC) $(CFLAGS) -o $@ -c $< 

.PHONY clean:
	rm -f *.o
	rm -f *.so


