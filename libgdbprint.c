#include "gdbprint.h"
#include <unistd.h>
#include <stdarg.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/ptrace.h>
#include <fcntl.h>
#include <errno.h>
#include <sys/wait.h>
#include <linux/limits.h>
#include <stdio.h>
#include <stdlib.h>

static char path[PATH_MAX];

#define TRUE 1
#define FALSE 0

static int gdb_check(void)
{
  int pid = fork();
  int status;
  int res;

  if (pid == -1) {
      return 0;
  }

  if (pid == 0) {
      int ppid = getppid();

      /* Child */
      if (ptrace(PTRACE_ATTACH, ppid, NULL, NULL) == 0) {
          /* Wait for the parent to stop and continue it */
          waitpid(ppid, NULL, 0);
          ptrace(PTRACE_CONT, NULL, NULL);

          /* Detach */
          ptrace(PTRACE_DETACH, getppid(), NULL, NULL);

          /* We were the tracers, so gdb is not present */
          res = FALSE;
      } else {
          /* Trace failed so gdb is present */
          res = TRUE;
      }
      exit(res);
  } else {
      waitpid(pid, &status, 0);
      res = WEXITSTATUS(status);
  }
  return res;
}


static int gdb_present(){
    return TRUE;
}
static FILE* create_open_pipe()
{
    int status, fd;
    FILE* ret;
    sprintf(path, "/tmp/gdb.%d", (int)getpid());
    status = mkfifo(path, S_IWUSR | S_IWGRP | S_IWOTH | S_IRUSR | S_IRGRP | S_IROTH);
    if ((-1 == status) && (errno != EEXIST)) {
        return NULL;
    }
    fd = open(path, O_NONBLOCK | S_IWUSR | S_IWGRP | S_IWOTH);
    ret = fdopen(fd, "a");
    setbuf(ret, NULL);
    return ret;
}

static FILE* gdb_pipe()
{
    static FILE* pipe = NULL;
    int debugger_present = gdb_present();

    if (debugger_present && !pipe) {
        pipe = create_open_pipe();
    }

    if (!debugger_present && pipe) {
        fclose(pipe);
        pipe = NULL;
        unlink(path);
    }
    return pipe;

}

int gdbprintf(const char* format, ...)
{
    int ret = -1;
    va_list args;
    va_start(args, format);
    FILE* stream = gdb_pipe();
    if (stream) {
        ret = vfprintf(stream, format, args);
    }
    va_end(args);
    return ret;
}
