#!/usr/bin/env python3
import gdb
from multiprocessing import Process, Pipe
import os
from select import select

class pipe_reader:
    def __init__(self):
        self.in_parent, self.in_child = (None, None)
        self.reader = None

    def handle_stop(self, ev):
        self.in_parent.send('')
        self.in_parent.close()
        self.reader.join()
    
    def handle_cont(self, ev):
        self.in_parent, self.in_child = Pipe()
        self.reader = Process(target = self.read_fifo)
        self.reader.start()

    def read_fifo(self):
        pid = gdb.selected_inferior().pid
        fifo_fd = os.open('/tmp/gdb.'+str(pid), os.O_NONBLOCK)
        fifo = open(fifo_fd)
        run = True
        while run:
            r, w, x = select([fifo, self.in_child], [], [])
            if fifo in r:
                print(fifo.read())

            if self.in_child in r:
                data = self.in_child.recv()
                run = False
        fifo.close()
        self.in_child.close()



reader=pipe_reader()
gdb.events.cont.connect(reader.handle_cont)
gdb.events.stop.connect(reader.handle_stop)
gdb.events.exited.connect(reader.handle_stop)

