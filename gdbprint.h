#ifndef GDDPRINT_H
#define GDBPRINT_H
#ifdef __cplusplus__
extern "C" {
#endif /* __cplusplus__ */

int gdbprintf(const char* format, ...);

#ifdef __cplusplus__
}
#endif /* __cplusplus__ */


#endif /* GDDPRINT_H */
